# EMiSE | ЕМПІ

Empirical methods in Software Engineering | Емпіричні методи програмної інженерії.

## :eight_spoked_asterisk: Про курс
ЕМПІ - курс для вивчення теорії та практики статистичного аналізу та ознайомлення з необхідними методами обробки експериментальних даних.

## :eight_spoked_asterisk: Призначення  
Курс призначений для оволодіння знаннями і практичними навичками виведення обгрунтованих висновків на основі зібраних та оброблених даних.
Отримані навики дозволяють створити чітку картину розстановки інформації певної досліджуваної галузі.

## :eight_spoked_asterisk: Структура курсу

Курс складається із N занять, кожне з яких представлене за допомогою [Jupyter Notebook'ів](https://en.wikipedia.org/wiki/Project_Jupyter#Jupyter_Notebook).

### Тематика занять:

Організаційне заняття:
* [Вступ. Про курс та середовище розробки](https://drive.google.com/open?id=141yLl71FMlCLWmZ_6g7uwUw62OQfnLWy&authuser=1)

Вступ до емпіричних методів у програміній інженерії:
* [Загальна характеристика емпіричних методів, основні поняття статистики та первинна обробка даних](https://drive.google.com/open?id=1gGA9HTcoVBqEYo6EEfM5EVOkBb9LZsch&authuser=1)
* [Генеральна сукупність і вибірка. Міри мінливості та квартилі розподілу](https://drive.google.com/open?id=1nMBfoMo8fZUCjEBE7DMvBDTOpwWs9hNU&authuser=1
)
* [Нормальний розподіл. Центральна гранична теорема та довірчі інтервали. Ідея статистичного висновку](https://drive.google.com/open?id=1KReEGTgrfUm6IqpIc1Lc4KpojHarbdJX&authuser=1)
* ... список буде поповнюватись по мірі релізу блокнотів.


## :eight_spoked_asterisk: Автори

Курс розроблений [групою авторів](AUTHORS) під керівництвом Alex Orlovskyi [(t.me)](https://t.me/alexoru4) [(mail)](mailto:orlovskyi.alex@gmail.com).

## :key: License

Проект ліцензовано згідно з умовами GNU General Public License, GPLv3 - ознайомтесь з деталями у файлі 
[LICENSE](LICENSE).